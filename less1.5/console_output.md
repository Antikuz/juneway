`antikuz@debian:~/JuneWay/less1.5$ docker run --rm --privileged --name dind -v $(pwd)/dind-test:/dind-test -d -p 8080:8081 less1.5_dind`

```sh
92ebcdb53476becd429899c3bc3741ab5c6f82e28e316a2f04e3fce38c16049d
```

`antikuz@debian:~/JuneWay/less1.5$ docker exec dind chmod +x /dind-test/nginx.sh`

`antikuz@debian:~/JuneWay/less1.5$ docker exec dind docker build -t alpine_strace /dind-test/.`

```sh
Sending build context to Docker daemon  3.072kB
Step 1/3 : FROM alpine:latest
latest: Pulling from library/alpine
29291e31a76a: Pulling fs layer
29291e31a76a: Verifying Checksum
29291e31a76a: Download complete
29291e31a76a: Pull complete
Digest: sha256:eb3e4e175ba6d212ba1d6e04fc0782916c08e1c9d7b45892e9796141b1d379ae
Status: Downloaded newer image for alpine:latest
 ---> 021b3423115f
Step 2/3 : RUN apk upgrade && apk add strace
 ---> Running in ebe2c6a8e266
fetch https://dl-cdn.alpinelinux.org/alpine/v3.14/main/x86_64/APKINDEX.tar.gz
fetch https://dl-cdn.alpinelinux.org/alpine/v3.14/community/x86_64/APKINDEX.tar.gz
OK: 6 MiB in 14 packages
(1/1) Installing strace (5.12-r0)
Executing busybox-1.33.1-r3.trigger
OK: 7 MiB in 15 packages
Removing intermediate container ebe2c6a8e266
 ---> 57d43e9818db
Step 3/3 : CMD ["strace", "strace"]
 ---> Running in 3696acd7cf07
Removing intermediate container 3696acd7cf07
 ---> 1c241e61833a
Successfully built 1c241e61833a
Successfully tagged alpine_strace:latest
```

`antikuz@debian:~/JuneWay/less1.5$ docker exec dind docker run alpine_strace`

```sh
execve("/usr/bin/strace", ["strace"], 0x7ffd1a9109b0 /* 3 vars */) = 0
arch_prctl(ARCH_SET_FS, 0x7f2effd42b48) = 0
set_tid_address(0x7f2effd42f90)         = 10
brk(NULL)                               = 0x55ef49998000
brk(0x55ef4999a000)                     = 0x55ef4999a000
mmap(0x55ef49998000, 4096, PROT_NONE, MAP_PRIVATE|MAP_FIXED|MAP_ANONYMOUS, -1, 0) = 0x55ef49998000
mprotect(0x7f2effd3f000, 4096, PROT_READ) = 0
mprotect(0x55ef48b8d000, 364544, PROT_READ) = 0
getpid()                                = 10
uname({sysname="Linux", nodename="394754b5eca8", ...}) = 0
open("/proc/sys/kernel/pid_max", O_RDONLY|O_LARGEFILE) = 3
read(3, "32768\n", 23)                  = 6
close(3)                                = 0
writev(2, [{iov_base="strace: must have PROG [ARGS] or"..., iov_len=40}, {iov_base=NULL, iov_len=0}], 2strace: must have PROG [ARGS] or -p PID
) = 40
writev(2, [{iov_base="Try 'strace -h' for more informa"..., iov_len=38}, {iov_base=NULL, iov_len=0}], 2Try 'strace -h' for more information.
) = 38
getpid()                                = 10
exit_group(1)                           = ?
+++ exited with 1 +++
```

`antikuz@debian:~/JuneWay/less1.5$ docker exec dind sh /dind-test/nginx.sh`

```sh
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
33847f680f63: Pulling fs layer
dbb907d5159d: Pulling fs layer
8a268f30c42a: Pulling fs layer
b10cf527a02d: Pulling fs layer
c90b090c213b: Pulling fs layer
1f41b2f2bf94: Pulling fs layer
b10cf527a02d: Waiting
c90b090c213b: Waiting
1f41b2f2bf94: Waiting
8a268f30c42a: Verifying Checksum
8a268f30c42a: Download complete
b10cf527a02d: Verifying Checksum
b10cf527a02d: Download complete
c90b090c213b: Verifying Checksum
c90b090c213b: Download complete
1f41b2f2bf94: Verifying Checksum
1f41b2f2bf94: Download complete
dbb907d5159d: Verifying Checksum
dbb907d5159d: Download complete
33847f680f63: Verifying Checksum
33847f680f63: Download complete
33847f680f63: Pull complete
dbb907d5159d: Pull complete
8a268f30c42a: Pull complete
b10cf527a02d: Pull complete
c90b090c213b: Pull complete
1f41b2f2bf94: Pull complete
Digest: sha256:8f335768880da6baf72b70c701002b45f4932acae8d574dedfddaf967fc3ac90
Status: Downloaded newer image for nginx:latest
28b6ef3dc0cd0d0851f6532059e6ff41866626f9a029f9fb361ba0ae86245a1b
```

`antikuz@debian:~/JuneWay/less1.5$ docker exec dind docker ps`

```sh
CONTAINER ID   IMAGE     COMMAND                  CREATED          STATUS          PORTS                  NAMES
28b6ef3dc0cd   nginx     "/docker-entrypoint.…"   31 seconds ago   Up 29 seconds   0.0.0.0:8081->80/tcp   great_gagarin
```

`antikuz@debian:~/JuneWay/less1.5$ curl -I http://127.0.0.1:8080`

```sh
HTTP/1.1 200 OK
Server: nginx/1.21.1
Date: Sun, 15 Aug 2021 09:54:36 GMT
Content-Type: text/html
Content-Length: 612
Last-Modified: Tue, 06 Jul 2021 14:59:17 GMT
Connection: keep-alive
ETag: "60e46fc5-264"
Accept-Ranges: bytes
```
